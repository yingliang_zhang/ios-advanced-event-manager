//
//  EventXMLParser.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 30/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "EventXMLParser.h"

@implementation EventXMLParser

-(id)init
{
    if(self = [super init])
    {
        self.eventList = [[NSMutableArray alloc]init];
    }
    return  self;
}

-(void)parseFromAddress:(NSString *)address
{
    NSURL* url = [NSURL URLWithString:address];
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    
    parser.delegate = self;
    [parser parse];
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"item"])
    {
        currentEvent = [[OnlineEvent alloc] init];
    }
    if ([elementName isEqualToString:@"enclosure"]) {
        self.photoURL = [attributeDict valueForKey:@"url"];
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (currentValue == nil)
    {
        currentValue = [NSMutableString stringWithString:string];
    }
    else
    {
        [currentValue appendString:string];
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSString* finalValue = [currentValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"item"])
    {
        [self.eventList addObject:currentEvent];
        currentEvent = nil;
    }
    if ([elementName isEqualToString:@"title"])
    {
        currentEvent.title = finalValue;
    }
    if ([elementName isEqualToString:@"link"])
    {
        currentEvent.link = finalValue;
    }
    if ([elementName isEqualToString:@"description"])
    {
        currentEvent.description = finalValue;
    }
    if ([elementName isEqualToString:@"pubDate"])
    {
        currentEvent.pubDate = finalValue;
    }
    if ([elementName isEqualToString:@"enclosure"])
    {
        currentEvent.imgURL = self.photoURL;
    }
    currentValue = nil;
}

@end
