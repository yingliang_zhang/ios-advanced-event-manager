//
//  EventDetailsController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 30/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "EventDetailsController.h"

@interface EventDetailsController ()

@end

@implementation EventDetailsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.titleLabel.text = [NSString stringWithFormat:@"%@", self.selectedEvent.title];
    self.pubdateLabel.text = [NSString stringWithFormat:@"Publish Date: %@", self.selectedEvent.pubDate];
    self.descriptionText.text = self.selectedEvent.description;
    self.eventImage.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.eventImage.layer.borderWidth = 3.0f;
    self.descriptionText.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.descriptionText.layer.borderWidth = 1.0f;
    self.descriptionText.layer.cornerRadius = 4.0f;
    NSData* imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.selectedEvent.imgURL]];
    self.eventImage.image = [UIImage imageWithData:imgData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"viewEventWebsite"])
    {
        EventWebsiteController* controller = segue.destinationViewController;
        controller.webAddress = self.selectedEvent.link;
    }
}

@end
