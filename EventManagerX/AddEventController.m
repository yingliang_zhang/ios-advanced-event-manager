//
//  AddEventController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 28/03/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "AddEventController.h"

@interface AddEventController ()

@end

@implementation AddEventController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.datePicker = [[UIDatePicker alloc] init];
    [self.datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    self.dateText.inputView = self.datePicker;
    self.people = [[NSMutableArray alloc]init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.distanceFilter = 10;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    self.anewEvent = (Event*)[NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"%@", self.selectedLocation);
    self.locationText.text = self.selectedLocation;
    
    // Make locationText textView corner radius.
    self.locationText.layer.cornerRadius = 8.0f;
}

-(IBAction)addPeople:(id)sender
{
    //Create a new people picker and set the view controller as its delegate.
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc]init];
    picker.peoplePickerDelegate = self;
    
    //Present the picker view controller.
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    //Dismiss the people picker when the user clicks cancel.
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    //Get the name of the people that the user has selected.
    NSString* fname = (__bridge_transfer NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lname = (__bridge_transfer NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
    
    NSString* fullName = [NSString stringWithFormat:@"%@ %@", fname, lname];
    [self.people addObject:fullName];
    NSString* names = [[NSString alloc]init];
    // Pass each name to the text of peopleText field.
    for (NSString* name in self.people) {  
        names = [names stringByAppendingFormat:@"%@, ", name];
        self.peopleText.text = names;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    return NO;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //CLLocation* loc = [locations lastObject];
    //currentLocation = loc.coordinate;
    
}

//  Get the location details using reverseGeoCodeLocation and assign the details to the location text field.
- (IBAction)insertCurrentLocation:(id)sender
{
    CLGeocoder* geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:locationManager.location completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark* placemark = [placemarks objectAtIndex:0];
        NSString * address = [NSString stringWithFormat:@"%@, %@, %@, %@",placemark.name, placemark.locality, placemark.administrativeArea, placemark.country];
        self.locationText.text = address;
        LocationAnnotation* locAn = placemark.location;
        self.lat = locAn.coordinate.latitude;
        self.lng = locAn.coordinate.longitude;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"chooseLocationSegue"])
    {
        LocationListController* controller = segue.destinationViewController;
        controller.managedObjectContext = self.managedObjectContext;
    }
    if ([segue.identifier isEqualToString:@"choosePhotoSegue"])
    {
        PhotoCollectionController* controller = segue.destinationViewController;
        controller.eventToEdit = self.anewEvent;
        controller.managedObjectContext = self.managedObjectContext;
    }
}

- (IBAction)saveEvent:(id)sender {
    //This part is from Elliot's Assignment 1 solution.
    //This boolean will be false if the validation fails.
    BOOL validated = YES;
    if ([self.titleText.text isEqualToString:@""])
    {
        //This sets the border of the text field to red if there is no content.
        self.titleText.layer.cornerRadius = 8.0f;
        self.titleText.layer.masksToBounds = YES;
        self.titleText.layer.borderColor = [[UIColor redColor]CGColor];
        self.titleText.layer.borderWidth = 1.5f;
        validated = NO;
    }
    
    if ([self.locationText.text isEqualToString:@""])
    {
        self.locationText.layer.masksToBounds = YES;
        self.locationText.layer.borderColor = [[UIColor redColor] CGColor];
        self.locationText.layer.borderWidth = 1.5f;
        validated = NO;
    }
    //If the validation passed
    if (validated)
    {
        self.anewEvent.title = self.titleText.text;
        self.anewEvent.comment = self.commentText.text;
        self.anewEvent.people = self.peopleText.text;
        self.anewEvent.date = self.dateText.text;
        self.anewEvent.location = self.locationText.text;
        self.anewEvent.lng = self.lng;
        self.anewEvent.lat = self.lat;
        
        NSError* error;
        if(![self.managedObjectContext save:&error])
        {
            NSLog(@"Core Data Save Error:%@", [error description]);
        }
        
        [self.delegate didSaveEvent:self.anewEvent];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        //If the validation failed, show an alert view informing the user.
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Notice!" message:@"Event title and location can not be blank." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

//  Dismiss the date picker and assign the date and time to the text field.
-(IBAction)textFieldDoneEditing:(id)sender
{
    [sender resignFirstResponder];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSDate* selected = [self.datePicker date];
    NSString* eventDate = [dateFormatter stringFromDate:selected];
    self.dateText.text = [NSString stringWithFormat:@"%@",eventDate];
}

// Touching the background to dismiss the keyboard and assign the date and time to the text field.
-(IBAction)backgroudClick:(id)sender
{
    [self.titleText resignFirstResponder];
    [self.peopleText resignFirstResponder];
    [self.locationText resignFirstResponder];
    [self.commentText resignFirstResponder];
    [self.dateText resignFirstResponder];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSDate* selected = [self.datePicker date];
    NSString* eventDate = [dateFormatter stringFromDate:selected];
    self.dateText.text = [NSString stringWithFormat:@"%@",eventDate];

}
@end
