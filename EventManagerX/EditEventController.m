//
//  EditEventController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 4/04/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "EditEventController.h"

@interface EditEventController ()

@end

@implementation EditEventController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.titleText.text = self.eventToEdit.title;
    self.peopleText.text = self.eventToEdit.people;
    self.commentText.text = self.eventToEdit.comment;
    self.locationText.text = self.eventToEdit.location;
    self.dateText.text = self.eventToEdit.date;
    self.datePicker = [[UIDatePicker alloc] init];
    [self.datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    self.dateText.inputView = self.datePicker;
    self.people = [[NSMutableArray alloc]init];
    self.lat = self.eventToEdit.lat;
    self.lng = self.eventToEdit.lng;
    
    //Add two bar buttons on the right side.
    self.saveButton = [[UIBarButtonItem alloc]
                       initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                       target:self action:@selector(editEvent:)];
    
    self.shareButton = [[UIBarButtonItem alloc]
                         initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                         target:self
                         action:@selector(shareEvent:)];
    
    self.navigationItem.rightBarButtonItems =
    [NSArray arrayWithObjects:self.saveButton,self.shareButton, nil];
    
    //Add border to the mapview.
    self.mapView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.mapView.layer.borderWidth = 3.0f;
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.distanceFilter = 10;
    locationManager.delegate = self;
    self.eventLocation = [[LocationAnnotation alloc] initWithTitle:self.eventToEdit.location subtitle:@"" lat:self.eventToEdit.lat andLong:self.eventToEdit.lng];
    [locationManager startUpdatingLocation];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.mapView removeAnnotation:[self.mapView.annotations lastObject]];
    [self addAnnotation:self.eventLocation];
    [self focusOn:self.eventLocation];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)addPeople:(id)sender
{
    //Create a new people picker and set the view controller as its delegate.
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc]init];
    picker.peoplePickerDelegate = self;
    
    //Present the picker view controller.
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    //Dismiss the people picker when the user clicks cancel.
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    //Get the name of the people that the user has selected.
    NSString* fname = (__bridge_transfer NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    NSString* lname = (__bridge_transfer NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
    
    NSString* fullName = [NSString stringWithFormat:@"%@ %@", fname, lname];
    [self.people addObject:fullName];
    NSString* names = [[NSString alloc]init];
    // Pass each name to the text of peopleText field.
    for (NSString* name in self.people) {
        names = [names stringByAppendingFormat:@"%@, ", name];
        self.peopleText.text = names;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    return NO;
}

// The method that edit the event.
- (IBAction)editEvent:(id)sender
{
    //This part is from Elliot Assignment 1 solution.
    //This boolean will be false if the validation fails.
    BOOL validated = YES;
    if ([self.titleText.text isEqualToString:@""])
    {
        //This sets the border of the text field to red if there is no content.
        self.titleText.layer.cornerRadius = 8.0f;
        self.titleText.layer.masksToBounds = YES;
        self.titleText.layer.borderColor = [[UIColor redColor]CGColor];
        self.titleText.layer.borderWidth = 1.5f;
        validated = NO;
    }
    
    if ([self.locationText.text isEqualToString:@""])
    {
        self.titleText.layer.cornerRadius = 8.0f;
        self.locationText.layer.masksToBounds = YES;
        self.locationText.layer.borderColor = [[UIColor redColor] CGColor];
        self.locationText.layer.borderWidth = 1.5f;
        validated = NO;
    }
    //If the validation passed
    if (validated)
    {
        Event* editEvent = self.eventToEdit;
        editEvent.title = self.titleText.text;
        editEvent.people = self.peopleText.text;
        editEvent.location = self.locationText.text;
        editEvent.comment = self.commentText.text;
        editEvent.date = self.dateText.text;
        editEvent.lat = self.lat;
        editEvent.lng = self.lng;
        
        [self.delegate didEditEvent:editEvent];
        [self.navigationController popViewControllerAnimated:YES];
        
        NSError* error;
        if(![self.managedObjectContext save:&error])
        {
            NSLog(@"Core Data Save Error: %@", [error description]);
        }
    }
    else
    {
        //If the validation failed, show an alert view informing the user.
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Notice!" message:@"Event title and location can not be blank." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"chooseLocationSegue"])
    {
        LocationListController* controller = segue.destinationViewController;
        controller.managedObjectContext = self.managedObjectContext;
    }
    if ([segue.identifier isEqualToString:@"choosePhotoSegue"])
    {
        PhotoCollectionController* controller = segue.destinationViewController;
        controller.eventToEdit = self.eventToEdit;
        controller.managedObjectContext = self.managedObjectContext;
    }
}

- (IBAction)insertCurrentLocation:(id)sender
{
    CLGeocoder* geoCoder = [[CLGeocoder alloc]init];
    [geoCoder reverseGeocodeLocation:locationManager.location completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark* placemark = [placemarks objectAtIndex:0];
        NSString * address = [NSString stringWithFormat:@"%@, %@, %@, %@",placemark.name, placemark.locality, placemark.administrativeArea, placemark.country];
        self.locationText.text = address;
        
        // Change current location annotation to customized LocationAnnotation.
        LocationAnnotation* locAn = [[LocationAnnotation alloc] initWithTitle:placemark.name subtitle:placemark.locality lat:placemark.location.coordinate.latitude andLong:placemark.location.coordinate.longitude];
        self.lat = locAn.coordinate.latitude;
        self.lng = locAn.coordinate.longitude;
        // Focus on current location.
        [self.mapView setShowsUserLocation:YES];
        [self focusOn:locAn];
    }];
}

//  Dismiss the date picker and assign the date and time to the text field.
-(IBAction)textFieldDoneEditing:(id)sender
{
    [sender resignFirstResponder];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSDate* selected = [self.datePicker date];
    NSString* eventDate = [dateFormatter stringFromDate:selected];
    self.dateText.text = [NSString stringWithFormat:@"%@",eventDate];
}

// Touching the background to dismiss the keyboard and assign the date and time to the text field.
-(IBAction)backgroudClick:(id)sender
{
    [self.titleText resignFirstResponder];
    [self.peopleText resignFirstResponder];
    [self.locationText resignFirstResponder];
    [self.commentText resignFirstResponder];
    [self.dateText resignFirstResponder];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSDate* selected = [self.datePicker date];
    NSString* eventDate = [dateFormatter stringFromDate:selected];
    self.dateText.text = [NSString stringWithFormat:@"%@",eventDate];
    
}

//The method that share the event to social networks.
- (IBAction)shareEvent:(id)sender
{
    //Initialize the format of the event that will be posted.
    NSString* postText = [NSString stringWithFormat:@"My Event:%@\nPeople Included:%@\nLocation:%@\nComments:%@\nDate:%@",self.titleText.text,self.peopleText.text,self.locationText.text,self.commentText.text,self.dateText.text];
    UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[postText] applicationActivities:nil];
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)addAnnotation:(id<MKAnnotation>)annotation
{
    [self.mapView addAnnotation:self.eventLocation];
}

-(void)focusOn:(id<MKAnnotation>)annotation
{
    //  When focus on the initialLocation, the map will be zoomed in of a certain range
    //  instead of an eagle view.
    MKCoordinateSpan span;
    span.longitudeDelta = 0.02;
    span.latitudeDelta = 0.02;
    
    MKCoordinateRegion region;
    region.span = span;
    
    self.mapView.region = region;
    self.mapView.centerCoordinate = annotation.coordinate;
    [self.mapView selectAnnotation:annotation animated:YES];
}


@end
