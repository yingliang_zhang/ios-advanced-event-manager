//
//  Photo.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 1/06/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "Photo.h"
#import "Event.h"


@implementation Photo

@dynamic photo;
@dynamic events;

@end
