//
//  EventDetailsController.h
//  EventManagerX
//
//  Manage the details of online events.
//
//  Created by Yingliang Zhang on 30/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "OnlineEvent.h"
#import "EventDetailsController.h"
#import "EventWebsiteController.h"

@interface EventDetailsController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel* titleLabel;
@property (strong, nonatomic) IBOutlet UILabel* pubdateLabel;
@property (strong, nonatomic) IBOutlet UITextView* descriptionText;
@property (strong, nonatomic) IBOutlet UIImageView* eventImage;

@property (strong, nonatomic) OnlineEvent* selectedEvent;

@end
