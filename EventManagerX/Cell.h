//
//  Cell.h
//  EventManagerX
//
//  This is the context of the cell that will be displayed on
//  the list, including title, people and date.
//
//  Created by Yingliang Zhang on 7/04/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *peopleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
