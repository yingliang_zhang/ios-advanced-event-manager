//
//  PhotoCollectionController.h
//  EventManagerX
//
//  Created by Yingliang Zhang on 11/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoCell.h"
#import "PhotoViewController.h"
#import "Event.h"
#import "Photo.h"

@interface PhotoCollectionController : UICollectionViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate,PhotoViewControllerDelegate>

@property (strong, nonatomic) NSMutableArray* imageList;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cameraButton;
@property int imgRow;
@property (strong,nonatomic) UIImage* img;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (strong, nonatomic) Event* eventToEdit;

-(IBAction)openCamera:(id)sender;

@end
