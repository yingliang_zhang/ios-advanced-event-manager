//
//  OnlineEventListController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 30/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "OnlineEventListController.h"

@interface OnlineEventListController ()

@end

@implementation OnlineEventListController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.onlineEvents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"EventCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    // Configure the cell...
    OnlineEvent* e = [self.onlineEvents objectAtIndex:indexPath.row];
    cell.textLabel.text = e.title;
    cell.detailTextLabel.text = e.description;
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:e.imgURL]];
    cell.imageView.image = [UIImage imageWithData:imageData];
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"viewEventDetails"])
    {
        EventDetailsController* controller = segue.destinationViewController;
        controller.selectedEvent = [self.onlineEvents objectAtIndex:[self.tableView indexPathForCell:sender].row];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
      *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
