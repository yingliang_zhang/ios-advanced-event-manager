//
//  ChooseMapViewController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 7/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "ChooseMapViewController.h"

@interface ChooseMapViewController ()

@end

@implementation ChooseMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.distanceFilter = 10;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.mapView setShowsUserLocation:YES];
    self.mapView.delegate = self;
}


-(void)mapView:(MKMapView*)mv didAddAnnotationViews:(NSArray *)views
{
    MKAnnotationView* annotationView = [views lastObject];
    id<MKAnnotation> mp = [annotationView annotation];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 5000, 5000);
    
    [mv setRegion:region animated:YES];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation* loc = [locations lastObject];
    currentLocation = loc.coordinate;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addAnnotation:(id<MKAnnotation>)annotation
{
    [self.mapView addAnnotation:annotation];
}

-(void)focusOn:(id<MKAnnotation>)annotation
{
    self.mapView.centerCoordinate = annotation.coordinate;
    [self.mapView selectAnnotation:annotation animated:YES];
}

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([segue.identifier isEqualToString:@"saveChosenLocationSegue"]) {
//        AddEventController* controller = segue.destinationViewController;
//        //  Assign the detail of the initialLocation to a string "address".
//        NSString * address = [NSString stringWithFormat:@"%@, %@",self.initialLocation.title,self.initialLocation.subtitle];
//       //  Pass the address to the property "selectedLocation" of "AddEventController".
//        controller.selectedLocation = address;
//    }
//}


//  Save the location to the location list.
-(IBAction)saveChosenLocation:(id)sender
{
    //AddEventController* controller = [self.navigationController.viewControllers objectAtIndex:1];
    self.latestLocation = [self.mapView.annotations objectAtIndex:0];
    
    //  Assign the detail of the initialLocation to a string "address".
    NSString * address = [NSString stringWithFormat:@"%@",self.latestLocation.title];
    NSLog(@"%@",address);
    //  Pass the address to the property "selectedLocation" of "AddEventController".
    //controller.selectedLocation = address;
    //controller.lat = self.latestLocation.coordinate.latitude;
    //controller.lng = self.latestLocation.coordinate.longitude;
    [self.delegate didAddAnnotation:self.latestLocation];
     //[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changeMapType:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex)
    {
        case 1: //Satellite
            self.mapView.mapType = MKMapTypeSatellite;
            break;
        case 2: //Hybrid
            self.mapView.mapType = MKMapTypeHybrid;
            break;
        default:  //Map
            self.mapView.mapType = MKMapTypeStandard;
    }
}


//  Long press to add annotation on the map, the user can only add one annotation at a time.
-(IBAction)longPressHappened:(UILongPressGestureRecognizer *)sender
{
    if(sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point = [sender locationInView:self.mapView];
        CLLocationCoordinate2D coor = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
        
        CLGeocoder* geoCoder = [[CLGeocoder alloc] init];
        CLLocation* loc = [[CLLocation alloc] initWithLatitude:coor.latitude longitude:coor.longitude];
        
        [geoCoder reverseGeocodeLocation:loc completionHandler:^(NSArray *placemarks, NSError *error)
         {
             CLPlacemark* pm = [placemarks objectAtIndex:0];
             
             LocationAnnotation* locAn = [[LocationAnnotation alloc] initWithTitle:[NSString stringWithFormat:@"%@, %@", pm.name, pm.locality] subtitle:[NSString stringWithFormat:@"%@ - %@",pm.administrativeArea, pm.country] lat:coor.latitude andLong:coor.longitude];
             
             // Remove the previous annotation when the user add a new one.
             for (int i = 0; i < [self.mapView.annotations count]; i++)
             {
                 [self.mapView removeAnnotation:[self.mapView.annotations objectAtIndex:i]];
                 NSLog(@"%i",[self.mapView.annotations count]);
             }                 
            [self.mapView addAnnotation:locAn];
         }];
    }
}

@end
