//
//  EditEventController.h
//  EventManagerX
//
//  Controller for user to edit existing events. It has the same functions with
//
//  Created by Yingliang Zhang on 4/04/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBookUI/AddressBookUI.h>
#import "LocationAnnotation.h"
#import "AddEventController.h"
#import "LocationListController.h"
#import "Event.h"
#import "PhotoCollectionController.h"


@protocol EditEventControllerDelegate <NSObject>

-(void)didSaveEvent:(Event*) event;
-(void)didEditEvent:(Event*) event;

@end

@interface EditEventController : UIViewController <CLLocationManagerDelegate,ABPeoplePickerNavigationControllerDelegate>
{
    CLLocationManager* locationManager;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) id<MKAnnotation> eventLocation;

@property IBOutlet UITextField* titleText;
@property IBOutlet UITextField* peopleText;
@property IBOutlet UITextField* locationText;
@property IBOutlet UITextField* commentText;
@property IBOutlet UITextField* dateText;
@property IBOutlet UIDatePicker *datePicker;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *shareButton;

@property double lat;
@property double lng;
@property (strong, nonatomic) NSString* selectedLocation;
@property (strong, nonatomic) NSMutableArray* people;// This property stores peoples that the user selected from the address books.

@property (strong, nonatomic) Event* eventToEdit;
@property (weak, nonatomic) id<EditEventControllerDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;

-(void)addAnnotation:(id<MKAnnotation>)annotation;
-(void)focusOn:(id<MKAnnotation>)annotation;
- (IBAction)saveEvent:(id)sender;
- (IBAction)editEvent:(id)sender;
- (IBAction)textFieldDoneEditing:(id)sender;
- (IBAction)backgroudClick:(id)sender;
- (IBAction)insertCurrentLocation:(id)sender;
- (IBAction)addPeople:(id)sender;
@end
