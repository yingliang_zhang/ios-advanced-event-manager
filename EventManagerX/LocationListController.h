//
//  LocationListController.h
//  EventManagerX
//
//  Created by Yingliang Zhang on 6/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"
#import "ChooseMapViewController.h"
#import "LocationAnnotation.h"
#import "AddEventController.h"
#import "EditEventController.h"
#import "Location.h"

@interface LocationListController : UITableViewController <ChooseMapViewControllerDelegate>

@property (strong, nonatomic) NSMutableArray* locationList;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (strong, nonatomic) MapViewController* mapViewController;

@end
