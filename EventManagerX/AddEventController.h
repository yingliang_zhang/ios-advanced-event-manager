//
//  AddEventController.h
//  EventManagerX
//
//  Controller for user to create new events.
//
//  Created by Yingliang Zhang on 28/03/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import <AddressBookUI/AddressBookUI.h>
#import "LocationAnnotation.h"
#import "LocationListController.h"
#import "PhotoCollectionController.h"

@protocol AddEventControllerDelegate <NSObject>

-(void)didSaveEvent:(Event*) event;

@end

@interface AddEventController : UIViewController
<CLLocationManagerDelegate, ABPeoplePickerNavigationControllerDelegate>
{
    CLLocationManager* locationManager;
    CLLocationCoordinate2D currentLocation;
    CLPlacemark* placemark;
}

@property IBOutlet UITextField* titleText;
@property IBOutlet UITextField* peopleText;
@property IBOutlet UITextField* commentText;
@property IBOutlet UITextField* dateText;
@property IBOutlet UIDatePicker *datePicker;
@property IBOutlet UITextView *locationText;

@property (strong, nonatomic) NSString* selectedLocation;// The property stores the location detail which the user has selected and saved in "MapViewController".
@property double lat;
@property double lng;
@property (weak, nonatomic) id<AddEventControllerDelegate> delegate;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (strong, nonatomic) NSMutableArray* people;// This property stores peoples that the user selected from the address books.
@property (strong, nonatomic) Event* anewEvent;

- (IBAction)saveEvent:(id)sender;
- (IBAction)textFieldDoneEditing:(id)sender;
- (IBAction)backgroudClick:(id)sender;
- (IBAction)insertCurrentLocation:(id)sender;
- (IBAction)addPeople:(id)sender;

@end
