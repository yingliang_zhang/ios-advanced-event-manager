//
//  LocationListController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 6/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "LocationListController.h"

@interface LocationListController ()

@end

@implementation LocationListController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //  Initialize default locations.
    self.locationList = [[NSMutableArray alloc]init];
    LocationAnnotation* locAn = [[LocationAnnotation alloc]initWithTitle:@"Monash Caulfield" subtitle:@"900,Dandenong Road, Melbourne" lat:-37.877623 andLong:145.045374];
    LocationAnnotation* locAn2 = [[LocationAnnotation alloc]initWithTitle:@"Monash Clayton" subtitle:@"Bld 51 Union Road Clayton, Melbourne" lat:-37.910896 andLong:145.133622];
    [self.locationList addObject:locAn];
    [self.mapViewController addAnnotation:locAn];
    [self.locationList addObject:locAn2];
    [self.mapViewController addAnnotation:locAn2];

    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription* eventDescription = [NSEntityDescription entityForName:@"Location" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:eventDescription];
    
    NSError* error;
    
    NSMutableArray* fetchResults = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    if(fetchResults !=nil)
    {
        for(Location* location in fetchResults)
        {
            LocationAnnotation* annotation = [[LocationAnnotation alloc] initWithTitle:location.title subtitle:location.subtitle lat:location.latitude andLong:location.longitude];
            [self.locationList addObject:annotation];
        }
    }
    else
    {
        NSLog(@"Core Data Fetch Error:%@", [error description]);
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)didSaveLocation:(LocationAnnotation *)annotation
{
    [self.locationList addObject:annotation];
    [self.tableView reloadData];
    [self.mapViewController addAnnotation:annotation];
}

-(void)didAddAnnotation:(LocationAnnotation *)annotation
{
    Location* location = (Location*)[NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:self.managedObjectContext];
    location.title = annotation.title;
    location.subtitle = annotation.subtitle;
    location.latitude = annotation.coordinate.latitude;
    location.longitude = annotation.coordinate.longitude;
    
    NSError* error;
    {
        if (!([self.managedObjectContext save:&error]))
        {
            NSLog(@"Core Data Save Error: %@", [error description]);
        }
    }
    [self.locationList addObject:annotation];
    [self.tableView reloadData];
}

-(void)didRomoveLocation:(LocationAnnotation*) annotation
{
    [self.locationList removeObject:annotation];
    [self.tableView reloadData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"chooseLocationSegue"])
    {
        ChooseMapViewController* controller = segue.destinationViewController;
        controller.delegate = self;
    }
    if ([segue.identifier isEqualToString:@"viewLocationSegue"])
    {
        MapViewController* controller = segue.destinationViewController;
        NSIndexPath* selectedIndex = [self.tableView indexPathForCell:sender];
        controller.initialLocation = [self.locationList objectAtIndex:selectedIndex.row];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [self.locationList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    LocationAnnotation* locAn = [self.locationList objectAtIndex:indexPath.row];
    
    cell.textLabel.text = locAn.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Lat: %f Long: %f", locAn.coordinate.latitude, locAn.coordinate.longitude];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.mapViewController focusOn:[self.locationList objectAtIndex:indexPath.row]];
    LocationAnnotation* locAn = [self.locationList objectAtIndex:indexPath.row];
    NSString * address = [NSString stringWithFormat:@"%@, %@",locAn.title,locAn.subtitle];
    
    // Get all the view controllers and set them into a NSArray
    NSArray *viewControllers = [[self navigationController] viewControllers];
    // Loop in the view controllers array to find the one we need
    for(int i=0; i<[viewControllers count]; i++)
    {
        id obj = [viewControllers objectAtIndex:i];
        // if the controller found is AddEventController, that means the user is adding a new event, so when the user taps the save button in MapViewController, it should jump back to AddEventController
        if([obj isKindOfClass:[AddEventController class]])
        {
            AddEventController* controller = obj;
            //  Pass the address to the property "selectedLocation" of "AddEventController".
            controller.selectedLocation = address;
            // Assign the latitude and longitude of the event to those of the location the user saved.
            controller.lat = locAn.coordinate.latitude;
            controller.lng = locAn.coordinate.longitude;
            [self.navigationController popToViewController:obj animated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        // if the controller found is EditEventController, that means the user is editing an existing event, so when the user taps the save button in MapViewController, it should jump back to EditEventController
        else if([obj isKindOfClass:[EditEventController class]])
        {
            EditEventController* controller = obj;
            // Set the text of the locationText field to address.
            controller.locationText.text = address;
            controller.lat = locAn.coordinate.latitude;
            controller.lng = locAn.coordinate.longitude;
            controller.eventLocation = locAn;
            [self.navigationController popToViewController:obj animated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        
    }
}

@end
