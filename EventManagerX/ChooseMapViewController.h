//
//  ChooseMapViewController.h
//  EventManagerX
//
//  The controller for user to choose a location on the map and save the location to the location list.
//
//  Created by Yingliang Zhang on 7/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationAnnotation.h"
//#import "AddEventController.h"

@protocol ChooseMapViewControllerDelegate <NSObject>

-(void)didAddAnnotation:(LocationAnnotation *)annotation;
-(void)didRomoveLocation:(LocationAnnotation*) annotation;

@end

@interface ChooseMapViewController : UIViewController
<MKMapViewDelegate,CLLocationManagerDelegate>
{
    CLLocationManager* locationManager;
    CLLocationCoordinate2D currentLocation;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) id<MKAnnotation> initialLocation;
@property (strong, nonatomic) LocationAnnotation* latestLocation;
@property (weak, nonatomic) id<ChooseMapViewControllerDelegate> delegate;

-(void)addAnnotation:(id<MKAnnotation>)annotation;
-(void)focusOn:(id<MKAnnotation>)annotation;
-(IBAction)changeMapType:(UISegmentedControl *)sender;
-(IBAction)longPressHappened:(UILongPressGestureRecognizer *)sender;
-(IBAction)saveChosenLocation:(id)sender;

@end
