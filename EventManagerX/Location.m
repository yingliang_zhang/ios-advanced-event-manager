//
//  Location.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 1/06/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "Location.h"
#import "Event.h"


@implementation Location

@dynamic title;
@dynamic subtitle;
@dynamic latitude;
@dynamic longitude;
@dynamic events;

@end
