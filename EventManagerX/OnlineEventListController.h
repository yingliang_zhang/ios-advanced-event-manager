//
//  OnlineEventListController.h
//  EventManagerX
//
//  Manage all the events syndicated online.
//
//  Created by Yingliang Zhang on 30/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnlineEvent.h"
#import "EventDetailsController.h"

@interface OnlineEventListController : UITableViewController

@property (strong, nonatomic) NSArray* onlineEvents;

@end
