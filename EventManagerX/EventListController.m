//
//  EventListController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 28/03/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "EventListController.h"
#import "Event.h"
#import "AddEventController.h"
#import "EditEventController.h"

@interface EventListController ()

@end

@implementation EventListController


-(void)didSaveEvent:(Event*)event
{
    [self.eventList addObject:event];
    [self startArraySort:@"date" isAscending:NO];
    [self.tableView reloadData];
}

-(void)didEditEvent:(Event*)event
{
    [self startArraySort:@"date" isAscending:NO];
    [self.tableView reloadData];
}



// Sort the event according to the date by ascending order.
-(void)startArraySort:(NSString*)keystring isAscending:(BOOL)isAscending
{
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:keystring ascending:isAscending];
    self.eventList = [[NSMutableArray alloc] initWithArray:[self.eventList sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]]];
}


- (void)viewDidLoad
{
    // Initialize two bar button on the right side.
    self.onlineEventButton = [[UIBarButtonItem alloc] initWithTitle:@"RSS" style:UIBarButtonItemStyleBordered target:self action:@selector(seeOnlineEvent:)];
    self.navigationItem.rightBarButtonItems =
    [NSArray arrayWithObjects:self.createEventButton,self.onlineEventButton, nil];
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription* eventDescription = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:eventDescription];
    
    NSError* error;
    
    NSMutableArray* fetchResults = [[self.managedObjectContext executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    if(fetchResults !=nil)
    {
        self.eventList = fetchResults;
        [self startArraySort:@"date" isAscending:NO];
    }
    else
    {
        NSLog(@"Core Data Fetch Error:%@", [error description]);
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"add"]) {
        AddEventController* controller = segue.destinationViewController;
        controller.delegate = self;
        controller.managedObjectContext = self.managedObjectContext;
    }
    if ([segue.identifier isEqualToString:@"edit"]) {
        EditEventController* controller = segue.destinationViewController;
        NSIndexPath* indexPath = [self.tableView indexPathForCell:sender];
        Event* event = [self.eventList objectAtIndex:indexPath.row];
        controller.delegate = self;
        controller.managedObjectContext = self.managedObjectContext;
        controller.eventToEdit = event;
    }
    
}

//  Display online events from the rss address.
-(IBAction)seeOnlineEvent:(id)sender
{
    NSString* address = @"http://www.eventfinder.com.au/feed/top-events-this-week.rss";
    EventXMLParser* parser = [[EventXMLParser alloc] init];
    
    [parser parseFromAddress:address];
    OnlineEventListController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"onlineEventList"];
    controller.onlineEvents = parser.eventList;
    //  Jump to the OnlineEventListController.
    [self.navigationController pushViewController:controller animated:YES];
}

/*
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.eventList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    Cell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    Event* event = [self.eventList objectAtIndex:indexPath.row];
    cell.titleLabel.text = event.title;
    cell.peopleLabel.text = event.people;
    cell.dateLabel.text = event.date;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSManagedObject* objectToDelete = [self.eventList objectAtIndex:indexPath.row];
        [self.managedObjectContext deleteObject:objectToDelete];
        
        [self.managedObjectContext save:nil];
        
        [self.eventList removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


@end
