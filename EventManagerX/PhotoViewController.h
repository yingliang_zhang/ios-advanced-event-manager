//
//  PhotoViewController.h
//  EventManagerX
//
//  Created by Yingliang Zhang on 11/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Photo.h"
//#import "PhotoCollectionController.h"

@protocol PhotoViewControllerDelegate <NSObject>

-(void)deletePhoto:(Photo*)photo;

@end

@interface PhotoViewController : UIViewController

@property (strong,nonatomic) Photo* photoToView;

@property (weak,nonatomic) IBOutlet UIImageView* photoView;
@property (weak, nonatomic)id<PhotoViewControllerDelegate> delegate;

-(IBAction)deleteThePhoto:(id)sender;
@end
