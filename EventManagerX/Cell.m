//
//  Cell.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 7/04/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "Cell.h"

@implementation Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.titleLabel.text = self;
        self.peopleLabel.text = self;
        self.dateLabel.text = self;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
