//
//  PhotoCollectionController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 11/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "PhotoCollectionController.h"

@interface PhotoCollectionController () {
//    NSArray* photos;
//    BOOL editEnabled;
}

@end

@implementation PhotoCollectionController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Initialize the camera button.
    self.cameraButton = [[UIBarButtonItem alloc]
                                             initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                             target:self
                                             action:@selector(openCamera:)];
    
    self.navigationItem.rightBarButtonItems =
    [NSArray arrayWithObjects:self.cameraButton, nil];
    
    self.imageList = [[self.eventToEdit.photos allObjects] mutableCopy];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.imageList count];
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"PhotoCell";
    PhotoCell* cell = (PhotoCell*)[self.collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Photo* photo = [self.imageList objectAtIndex:indexPath.row];
    NSData *imgData = photo.photo;
    UIImage* img = [UIImage imageWithData:imgData];
    cell.photoView.image = img;
    
    //cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"selectedbackgrond.png"]];
    
    return cell;
}

-(IBAction)openCamera:(id)sender
{
    UIImagePickerController* imagePicker = [[UIImagePickerController alloc]init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    imagePicker.delegate = self;
    [self.navigationController presentViewController:imagePicker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage* img = [info valueForKey:UIImagePickerControllerOriginalImage];
    if (img != nil) {
        NSData* imgData = [NSData dataWithData:UIImagePNGRepresentation(img)];
        Photo *photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:self.managedObjectContext];
        photo.photo = imgData;
        
        [self.eventToEdit addPhotosObject:photo];
        [self.imageList addObject:photo];
        
        NSError* error;
        if(![self.managedObjectContext save:&error])
        {
            NSLog(@"Core Data Save Error:%@", [error description]);
        }
        
        
        [self.collectionView reloadData];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ViewPhotoSegue"]) {
        PhotoViewController* controller = segue.destinationViewController;
        NSIndexPath* indexPath = [self.collectionView indexPathForCell:sender];
        controller.photoToView = [self.imageList objectAtIndex:indexPath.row];
        controller.delegate = self;
        self.img = controller.photoToView;
    }
}

-(void)deletePhoto:(Photo *)photo
{
    [self.managedObjectContext deleteObject:photo];
    [self.imageList removeObject:photo];
    
    NSError* error;
    if(![self.managedObjectContext save:&error])
    {
        NSLog(@"Core Data Save Error:%@", [error description]);
    }
    [self.collectionView reloadData];
}

@end
