//
//  DatePickerViewController.h
//  EventManagerX
//
//  Created by Yingliang Zhang on 29/03/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePickerViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
-(IBAction)buttonPressed;

@end
