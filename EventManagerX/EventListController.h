//
//  EventListController.h
//  EventManagerX
//
//  Manage all the events that the user created.
//
//  Created by Yingliang Zhang on 28/03/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "AddEventController.h"
#import "EditEventController.h"
#import "OnlineEventListController.h"
#import "Cell.h"
#import "EventXMLParser.h"

@interface EventListController : UITableViewController <AddEventControllerDelegate>

@property (strong, nonatomic) NSMutableArray* eventList;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *createEventButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *onlineEventButton;

-(IBAction)seeOnlineEvent:(id)sender;


@end
