//
//  PhotoCell.h
//  EventManagerX
//
//  Created by Yingliang Zhang on 11/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell

@property (weak,nonatomic) IBOutlet UIImageView* photoView;

@end
