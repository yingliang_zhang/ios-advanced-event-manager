//
//  EventWebsiteController.h
//  EventManagerX
//
//  Show the website of online events.
//
//  Created by Yingliang Zhang on 31/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventWebsiteController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView* webView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem* backButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem* forwardButton;

@property (strong, nonatomic) NSString* webAddress;

-(IBAction)goBack:(id)sender;
-(IBAction)goForward:(id)sender;

@end
