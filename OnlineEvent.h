//
//  OnlineEvent.h
//  EventManagerX
//
//  Created by Yingliang Zhang on 30/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnlineEvent : NSObject

@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic)  NSString* link;
@property (strong, nonatomic) NSString* description;
@property (strong, nonatomic) NSString* pubDate;
@property (strong, nonatomic) NSString* imgURL;


@end
