//
//  EventXMLParser.h
//  EventManagerX
//
//  Created by Yingliang Zhang on 30/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OnlineEvent.h"

@interface EventXMLParser : NSObject <NSXMLParserDelegate>
{
    NSXMLParser* parser;
    OnlineEvent* currentEvent;
    NSMutableString* currentValue;
}

@property (strong,nonatomic) NSMutableArray* eventList;
@property (strong, nonatomic) NSString* photoURL;

-(void)parseFromAddress:(NSString*)address;

@end
