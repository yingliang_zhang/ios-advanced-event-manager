//
//  LocationAnnotation.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 6/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "LocationAnnotation.h"

@implementation LocationAnnotation

-(id)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle lat:(double)lat andLong:(double)lng
{
    if(self = [super init])
    {
        self.title = title;
        self.subtitle = subtitle;
        CLLocationCoordinate2D loc;
        loc.latitude = lat;
        loc.longitude = lng;
        self.coordinate = loc;
    }
    return self;
}

@end
