//
//  EventWebsiteController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 31/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "EventWebsiteController.h"

@interface EventWebsiteController ()

@end

@implementation EventWebsiteController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSURL* url = [NSURL URLWithString:self.webAddress];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    self.webView.delegate = self;
    [self.webView loadRequest:request];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.backButton.enabled = webView.canGoBack;
    self.forwardButton.enabled = webView.canGoForward;
}

-(IBAction)goBack:(id)sender
{
    [self.webView goBack];
}

-(IBAction)goForward:(id)sender
{
    [self.webView goForward];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
