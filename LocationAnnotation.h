//
//  LocationAnnotation.h
//  EventManagerX
//
//  Created by Yingliang Zhang on 6/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LocationAnnotation : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* subtitle;

-(id)initWithTitle:(NSString*)title subtitle:(NSString*)subtitle lat:(double)lat andLong:(double)lng;

@end
