//
//  Event.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 1/06/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "Event.h"
#import "Location.h"
#import "Photo.h"


@implementation Event

@dynamic comment;
@dynamic date;
@dynamic lat;
@dynamic lng;
@dynamic location;
@dynamic people;
@dynamic title;
@dynamic locations;
@dynamic photos;

@end
