//
//  Event.h
//  EventManagerX
//
//  Created by Yingliang Zhang on 1/06/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location, Photo;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * date;
@property double lat;
@property double lng;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * people;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *locations;
@property (nonatomic, retain) NSSet *photos;
@end

@interface Event (CoreDataGeneratedAccessors)

- (void)addLocationsObject:(Location *)value;
- (void)removeLocationsObject:(Location *)value;
- (void)addLocations:(NSSet *)values;
- (void)removeLocations:(NSSet *)values;

- (void)addPhotosObject:(Photo *)value;
- (void)removePhotosObject:(Photo *)value;
- (void)addPhotos:(NSSet *)values;
- (void)removePhotos:(NSSet *)values;

@end
