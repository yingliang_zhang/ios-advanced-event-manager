//
//  MapViewController.h
//  EventManagerX
//
//  This is the controller of the map view to which the user will jumped to when he/she
//  has select a location on the location list. The location that the userd selected will
//  be displayed on the map and the user can change the map type. When user click the save
//  button on the right corner, the location will be saved back to "AddEventController".
//
//  Created by Yingliang Zhang on 6/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationAnnotation.h"
//#import "AddEventController.h"
//#import "EditEventController.h"

@class AddEventController;
@class EditEventController;

@interface MapViewController : UIViewController
<CLLocationManagerDelegate>
{
    CLLocationManager* locationManager;
    //CLLocationCoordinate2D currentLocation;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) id<MKAnnotation> initialLocation;

-(void)addAnnotation:(id<MKAnnotation>)annotation;
-(void)focusOn:(id<MKAnnotation>)annotation;
-(IBAction)changeMapType:(UISegmentedControl *)sender;
-(IBAction)saveLocationToEvent:(id)sender;

@end
