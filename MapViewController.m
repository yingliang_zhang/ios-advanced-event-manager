//
//  MapViewController.m
//  EventManagerX
//
//  Created by Yingliang Zhang on 6/05/13.
//  Copyright (c) 2013 Yingliang Zhang. All rights reserved.
//

#import "MapViewController.h"
#import "LocationAnnotation.h"
#import "AddEventController.h"
#import "EditEventController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    locationManager.distanceFilter = 10;
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
	// Do any additional setup after loading the view.
}

//-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
//{
//    CLLocation* loc = [locations lastObject];
//    currentLocation = loc.coordinate;
//}

-(void)viewDidAppear:(BOOL)animated
{
    [self addAnnotation:self.initialLocation];
    [self focusOn:self.initialLocation];
    //[self.mapView setShowsUserLocation:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addAnnotation:(id<MKAnnotation>)annotation
{
    [self.mapView addAnnotation:annotation];
}

-(void)focusOn:(id<MKAnnotation>)annotation
{
    //  When focus on the initialLocation, the map will be zoomed in of a certain range
    //  instead of an eagle view.
    MKCoordinateSpan span;
    span.longitudeDelta = 0.2;
    span.latitudeDelta = 0.2;
    
    MKCoordinateRegion region;
    region.span = span;
    
    self.mapView.region = region;
    self.mapView.centerCoordinate = annotation.coordinate;
    [self.mapView selectAnnotation:annotation animated:YES];
}

//  Save the location directly to the event from the mapview.
-(IBAction)saveLocationToEvent:(id)sender
{
    NSString * address = [NSString stringWithFormat:@"%@, %@",self.initialLocation.title,self.initialLocation.subtitle];
    // Get all the view controllers and set them into a NSArray
    NSArray *viewControllers = [[self navigationController] viewControllers];
    // Loop in the view controllers array to find the one we need
    for(int i=0; i<[viewControllers count]; i++)
    {
        id obj = [viewControllers objectAtIndex:i];
        // if the controller found is AddEventController, that means the user is adding a new event, so when the user taps the save button in MapViewController, it should jump back to AddEventController
        if([obj isKindOfClass:[AddEventController class]])
        {
            AddEventController* controller = obj;
            //  Pass the address to the property "selectedLocation" of "AddEventController".
            controller.selectedLocation = address;
            // Assign the latitude and longitude of the event to those of the location the user saved.
            controller.lat = self.initialLocation.coordinate.latitude;
            controller.lng = self.initialLocation.coordinate.longitude;
            [self.navigationController popToViewController:obj animated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        // if the controller found is EditEventController, that means the user is editing an existing event, so when the user taps the save button in MapViewController, it should jump back to EditEventController
        else if([obj isKindOfClass:[EditEventController class]])
        {
            EditEventController* controller = obj;
            // Set the text of the locationText field to address.
            controller.locationText.text = address;
            controller.lat = self.initialLocation.coordinate.latitude;
            controller.lng = self.initialLocation.coordinate.longitude;
            controller.eventLocation = self.initialLocation;
            [self.navigationController popToViewController:obj animated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        
    }
    
}

- (IBAction)changeMapType:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex)
    {
        case 1: //Satellite
            self.mapView.mapType = MKMapTypeSatellite;
            break;
        case 2: //Hybrid
            self.mapView.mapType = MKMapTypeHybrid;
            break;
        default:  //Map
            self.mapView.mapType = MKMapTypeStandard;
    }
}
@end